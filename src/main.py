import logging

import trio

import relax.client
import relax.rtm
import relax.ui


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


async def main():
    client = relax.client.get_client()
    logger.debug('initialized')
    async with trio.open_nursery() as nursery:
        nursery.start_soon(relax.rtm.read, client)
        nursery.start_soon(relax.ui.ui_loop)


if __name__ == '__main__':
    trio.run(main)
