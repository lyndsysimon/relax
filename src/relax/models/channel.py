# TODO: This should be a dataclass
class Channel:
    def __init__(self, name, **kwargs):
        self.name = name


def get_channels(client):
    channels = client.api_call(
        'channels.list',
        exclude_archived=True,
        exclude_members=True,
    ).get('channels')

    return [Channel(c['name']) for c in channels]


if __name__ == '__main__':
    from relax.client import get_client

    client = get_client()
    get_channels(client)



