from asciimatics.scene import Scene
from asciimatics.screen import Screen
from asciimatics.widgets import Frame


class ChannelList(Frame):
    def __init__(self, screen):
        super().__init__(
            screen,
            screen.height,
            screen.width * 1 // 5,
            title='Channels',
        )


def render(screen):
    scenes = (
        Scene(
            (ChannelList(screen), ),
            -1,
            name='Channels',
        ),
    )

    screen.play(
        scenes,
        stop_on_resize=True,
    )


async def ui_loop():
    while True:
        Screen.wrapper(
            render,
        )

