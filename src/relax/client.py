import os

from slackclient import SlackClient


def get_client(token=None):
    if token is None:
        token = os.environ['SLACK_API_TOKEN']
    return SlackClient(token)
