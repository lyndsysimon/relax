import logging

import trio


logger = logging.getLogger(__name__)


async def read(client):
    """
    Read the Slack API websocket connection
    """
    if client.rtm_connect():
        while client.server.connected is True:
            logger.info('tick')
            for message in client.rtm_read():
                await process_message(message)
            await trio.sleep(1)
    else:
        # TODO: raise a proper exception
        pass


async def process_message(message):
    _type = message.get('type')
    if _type == 'hello':
        print('hello!')
    else:
        logger.warn('Unhandled message type: {}'.format(_type))
