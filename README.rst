Relax: A lightweight, asynchronous Slack TUI
============================================

Running
-------

..

    pipenv run python src/main.py

Development Roadmap
-------------------

- Build out initial MVC pattern to retrieve and display the channel list
- Refactor RTM message handling to use a parser class

